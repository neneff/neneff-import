<?php

namespace Neneff\Import\Excel;


use Neneff\Import\AbstractImport;
use Neneff\Tools\Log;
use Neneff\Tools\Profiler;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

abstract class AbstractImportExcel extends AbstractImport
{
    /** @var array */
    protected $_templateHeader = [];

    /** @var array */
    protected $_extractedHeader = [];

    /** @var String | Integer */
    protected $_sheetIdentifier = 0;

    /** @var Integer */
    protected $_headerLine = 1;

    /** @var Integer */
    protected $_firstLine = 2;

    /** @var Integer */
    protected $_firstColumn = 'A';

    /** @var Integer <p>set to null to auto evaluating the size</p> */
    protected $_lastColumn = null;

    /** @var Worksheet $sheet */
    private $_sheet = null;


    /**
     * AbstractImportExcel constructor. Read and validate the File
     *
     * @param \PDO $pdo
     * @param $path
     *
     * @throws \Exception
     */
    public function __construct(\PDO $pdo, $path)
    {
        parent::__construct($pdo);

        // -- initialize
        $reader = IOFactory::createReaderForFile($path);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($path);

        if(is_int($this->_sheetIdentifier)) {
            $sheet = $spreadsheet->getSheet($this->_sheetIdentifier);
        }
        else if(is_string($this->_sheetIdentifier)) {
            $sheet = $spreadsheet->getSheetByName($this->_sheetIdentifier);
        }
        else {
            $this->addLog(Log::$LOG_WARNING, "No sheet available with identifier \"{$this->_sheetIdentifier}\" ");
            $sheet = $spreadsheet->getActiveSheet();
        }

        if(!$sheet) {
            throw new \Exception("Unable to perform the importation, there is no available worksheet");
        }
        $this->addLog(Log::$LOG_MESSAGE, "Initializing extraction of {$path}");

        // -- if there no last column at this autotest with the file
        if($this->_lastColumn === null) {
            $this->_lastColumn = $sheet->getHighestColumn();
        }

        $this->addLog(Log::$LOG_DEBUG, 'after excel load - memory_get_usage: '.Profiler::byteToMbyte(memory_get_usage(true)));

        // -- extract the file header data
        $this->_extractedHeader = $this->_extractHeader($sheet);

        if(!$this->_validateTemplateHeader($this->_extractedHeader))
        {
            $this->addLog(Log::$LOG_ERROR, "Importation fail - wrong header received", [
                'excepted' => json_encode($this->_templateHeader),
                'received' => json_encode($this->_extractedHeader),
            ]);
            throw new \Exception("Importation fail - wrong template Headers received");
        }

        // -- sheet to be used in the import
        $this->_sheet = $sheet;
    }


    /**
     * @param  array $row
     * @return array
     */
    protected function _processRow($row)
    {
        return $row;
    }

    /**
     * launch the import process
     */
    final public function import()
    {
        // -- pre-process
        $this->_importInit();

        // -- bound
        $firstColumnIndex = Coordinate::columnIndexFromString($this->_firstColumn);
        $lastColumnIndex  = Coordinate::columnIndexFromString($this->_lastColumn);
        $offset           = $firstColumnIndex;

        // -- extraction for import
        $data       = [];
        $limit      = 50;
        $emptyLines = 0;

        foreach($this->_sheet->getRowIterator($this->_firstLine) as $rowIndex => $row)
        {
            $emptyLine = true;
            $rowData   = array_fill(0, $lastColumnIndex-$offset, null);

            for($cellIndex=$firstColumnIndex; $cellIndex<=$lastColumnIndex; $cellIndex++)
            {
                $cell     = $this->_sheet->getCellByColumnAndRow($cellIndex, $rowIndex);
                $cellData = trim($cell->getValue());
                $cellData = ($cellData != '') ? $cellData : null;
                $rowData[$cellIndex -$offset] = $cellData;

                if($cellData && ($cellData != '')) {
                    $emptyLine = false;
                }
            }

            // -- filter empty lines from the data set
            if(!$emptyLine)
            {
                // -- add line to the current set
                foreach($this->_processRow($rowData) as $datum) {
                    $data[] = $datum;
                }
                $emptyLines = 0;
            }
            else {
                $emptyLines ++;
            }

            // -- when a chunk is ready
            // -- import it and reset the current chunk
            if((count($data) > 10000)) {
                $this->_importChunk($data);
                $data  = [];
            }

            // -- stop read once there are no more line
            if($emptyLines > $limit) {
                break;
            }
        }

        // -- import once more if there is remaining data
        if(count($data) > 0) {
            $this->_importChunk($data);
        }

        // -- post proccess
        $this->_importEnd();
    }

    /**
     * when start import process from file
     */
    protected function _importInit() {}

    /**
     * Execute the import
     * You should import your data into you tool from here
     *
     * @param array $chunk <p>Chunk of data to be imported</p>
     */
    abstract protected function _importChunk($chunk);

    /**
     * when file end and the import process end
     */
    protected function _importEnd() {}

    /**
     * @param Worksheet $sheet
     * @return array
     */
    protected function _extractHeader(Worksheet $sheet)
    {
        $headerExcelLine = $this->_headerLine;
        return $sheet->rangeToArray("{$this->_firstColumn}{$headerExcelLine}:{$this->_lastColumn}{$headerExcelLine}");
    }


    /**
     * check template header with the current header you extract from the file you import
     * @param  array   $extractedHeaders
     * @return Boolean
     */
    protected function _validateTemplateHeader(array $extractedHeaders)
    {
        foreach($this->_templateHeader as $rowIndex => $header)
        {
            foreach($header as $cellIndex => $value)

                if(!isset($extractedHeaders[$rowIndex][$cellIndex]) || (strtoupper(trim($extractedHeaders[$rowIndex][$cellIndex])) != strtoupper(trim($value))))
                {
                    return false;
                }
        }
        return true;
    }

}